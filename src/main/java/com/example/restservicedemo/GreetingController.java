package com.example.restservicedemo;

import com.example.randomnamedemo.RandomNameDemo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/")
	private @ResponseBody String greeting() {
		return "Hello, World!";
	}

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		if(name.equalsIgnoreCase("random")) {
			name = RandomNameDemo.getName();
		}

		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}
}