package com.example.restservicedemo.unit;

import com.example.restservicedemo.Greeting;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class GreetingTest {
    @Test
    public void testGreetingClass() {
        int testId = 99;
        String testContent = "Sample content";
        Greeting tester = new Greeting(testId, testContent);

        assertEquals(testId, tester.getId());
        assertEquals(testContent, tester.getContent());
    }
}
