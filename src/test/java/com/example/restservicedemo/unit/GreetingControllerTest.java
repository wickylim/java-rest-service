package com.example.restservicedemo.unit;

import com.example.restservicedemo.Greeting;

import com.example.restservicedemo.GreetingController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GreetingControllerTest {
    @Test
    public void testGreetingController() {
        GreetingController tester = new GreetingController();
        Object result = tester.greeting("User");

        assertEquals(Greeting.class, result.getClass());
        assertTrue(((Greeting)result).getId() >= 1);
        assertEquals("Hello, User!", ((Greeting)result).getContent());
    }
}
