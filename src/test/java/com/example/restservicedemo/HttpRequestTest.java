package com.example.restservicedemo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturnMessage() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:"+port+"/",String.class)).contains("Hello, World!");
        assertThat(this.restTemplate.getForObject("http://localhost:"+port+"/greeting",String.class)).contains("Hello, World!");
        assertThat(this.restTemplate.getForObject("http://localhost:"+port+"/greeting?name=User",String.class)).contains("Hello, User!");
    }
}
