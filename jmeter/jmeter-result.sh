#!/bin/bash

set -e

printf -v var "%.2f" "$1"
errorPct=$(jq .Total.errorPct jmeter-report/statistics.json)
printf -v errorPct "%.2f" "$errorPct"

if [[ $(echo "$errorPct $var" | awk '{print ($1 > $2)}') == 1 ]]; then
  echo Jmeter error % of "$errorPct", is more than "$var"
  exit 1
fi