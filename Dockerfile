FROM openjdk:14-jdk-alpine
RUN addgroup -S gitlab && adduser -S gitlab -G gitlab
USER gitlab:gitlab
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]